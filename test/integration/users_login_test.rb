require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  # using the user.yml test fixture you now can run:
  # bundle exec rake test TEST=test/integration/users_login_test.rb \
  # TESTOPTS="--name test_login_with_valid_information"
  # This didn't work for me...
  def setup
    @user = users(:michael)
  end
  
  #run this with bundle exec rake test TEST=test/integration/users_login_test.rb
  test "login with valid information followed by logout" do
    get login_path
    post login_path, session: { email: @user.email, password: 'password' }
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window.
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
  
  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token'] #symbol keys do not work with cookies
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token'] #symbol keys do not work with cookies
  end
end
