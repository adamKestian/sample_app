# Ruby on Rails Tutorial: "toy app!"

This is the first application for the
[*Ruby on Rails Tutorial*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

Heroku:

https://sleepy-chamber-8386.herokuapp.com/ | https://git.heroku.com/sleepy-chamber-8386.git